import Piece from "../pieces/Piece";

type Grid = Array<Array<Piece | null>>;
